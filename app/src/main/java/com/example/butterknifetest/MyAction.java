package com.example.butterknifetest;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;

public class MyAction implements ButterKnife.Action<TextView> {
    @Override
    public void apply(@NonNull TextView view, int index) {
        if(view.getVisibility() == View.VISIBLE)
            view.setVisibility(View.INVISIBLE);
        else
            view.setVisibility(View.VISIBLE);
    }
}
