package com.example.butterknifetest;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.textViewMain)
    TextView textView;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.switch1)
    Switch switch1;

    @BindViews({R.id.textView1, R.id.textView2, R.id.textView3})
    List<TextView> textViewList;
    @BindView(R.id.button2)
    Button button2;

    @BindString(R.string.Title) String string;
    @BindColor(R.color.red) int color;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Log.d("Resources: ", string + " " + color);

        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        switch1.setOnCheckedChangeListener((buttonView, isChecked) -> textView.setText(String.valueOf(switch1.isChecked())));
    }

    @OnClick({R.id.button, R.id.button2})
    public void onClickListener(Button btn){
        switch(btn.getId()){
            case R.id.button:
                textView.setText("button clicked");
                break;
            case R.id.button2:
                ButterKnife.apply(textViewList,new MyAction());
                break;
        }
    }
}
