package com.example.butterknifetest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                return Fragment1.newInstance();

            case 1:
                return Fragment2.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
